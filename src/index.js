import { Home, Nav, Gallery } from "./components";

const root = document.getElementById("root");

/**
 * @param {HTMLElement|Promise<HTMLElement>} element
 * @param {HTMLElement} [root=HTMLBodyElement]
 */
const render = async (element, parent = root, clear = false) => {
  if (clear) parent.innerHTML = "";

  const resolvedElement = await Promise.resolve(element);
  parent.appendChild(resolvedElement);
};

/** @returns {"HOME"|"ABOUT"|"TRADITIONAL"|"DIGITAL"|"DESIGN"} */
const getHash = () => (window.location.hash || "HOME").toUpperCase().substr(1);

const route = async () => {
  await render(Nav(), root, true);
  switch (getHash()) {
    case "ABOUT":
      await render(Home()); // TODO Render About
      break;
    case "TRADITIONAL":
      await render(Gallery("traditional"));
      break;
    case "DIGITAL":
      await render(Gallery("digital"));
      break;
    case "DESIGN":
      await render(Gallery("design"));
      break;
    case "HOME":
    default:
      render(Home());
  }
}

route();

window.addEventListener("hashchange", route);
