import firebase from "firebase";
import "firebase/storage";

// Initialize Firebase
firebase.initializeApp({
  apiKey: "AIzaSyAAqTp0NC63X4LnakCRfXCnf5KScS13WyI",
  authDomain: "art-portfolio-47b2c.firebaseapp.com",
  databaseURL: "https://art-portfolio-47b2c.firebaseio.com",
  projectId: "art-portfolio-47b2c",
  storageBucket: "art-portfolio-47b2c.appspot.com",
  messagingSenderId: "198734580548",
  appId: "1:198734580548:web:9a2cdf975249c55988e511",
  measurementId: "G-DNY6EN5EY6",
});

export const auth = firebase.auth();
export const storage = firebase.storage();
export const firstore = firebase.firestore();
