import { auth, storage, firstore } from "./firebase";
import { nanoid } from "nanoid"

const saveDetails = (id, categoresizeBy, title, medium, desc) => {
  // TODO
}

/**
 * @param {Object} FormEvent
 * @param {object} FormEvent.target - target of form submit event
 * @param {HTMLProgressElement} progressBar
 */
const upload = (
  { target: { file: fileInput, category, title, medium, desc } },
  progressBar
) => {

  // TODO Auth precheck
  // if (!auth.currentUser) {
  //   console.log("Not Logged In")
  //   return;
  // }

  // get upload file 
  const file = fileInput.files[0];
  
  // generate new ID
  const fid = nanoid();

  // build new file destination
  const destinationRef = storage.ref(`${category.value || "misc"}/${file.name}_${fid}`);

  // upload file
  const uploadTask = destinationRef.put(file);

  // update user on file upload progress
  uploadTask.on(
    "state_changed",
    (snapshot) => {
      progressBar.value =
        (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
    },
    (error) => {
      console.log(error);
    },
    () => {
      console.log("Upload Complete");
    }
  );

  saveDetails(fid, category, title, medium, desc)
};



export { upload };
