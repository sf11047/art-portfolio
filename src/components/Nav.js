// <li> <a href> text </a> </li>
const navLink = (linkText) => {
  const listElem = document.createElement("li");
  const link = document.createElement("a");
  link.textContent = linkText;
  link.setAttribute("href", `#${linkText}`);
  listElem.append(link);
  return listElem;
};

const Nav = () => {
  const nav = document.createElement("div");

  const title = document.createElement("h1");
  title.textContent = "sam f.";
  const navBar = document.createElement("div");
  const navLinks = document.createElement("ul");
  navLinks.className = "links";
  navLinks.append(
    navLink("home"),
    navLink("about"),
    navLink("traditional"),
    navLink("digital"),
    navLink("design")
  );
  navBar.append(navLinks);

  nav.append(title);
  nav.append(navBar);

  return nav;
};

export default Nav;
