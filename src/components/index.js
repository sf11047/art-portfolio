import Home from './Home'
import Nav from './Nav'
import Gallery from './Gallery'

export { Home, Nav, Gallery }