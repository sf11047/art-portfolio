import { storage } from "../firebase";

/**
 * @param {string} path - directory path of storage reference
 * @returns {Promise<string[]>} list of image URLs from given firebase storage path
 */
const getImageUrls = async (path) => {
  // Get all file references from storage
  const { items } = await storage.ref(path).listAll();
  let arr = [];
  for (let i = 0; i < items.length; i++) {
    // Add file reference "download URL" to array
    const url = await items[i].getDownloadURL();
    arr.push(url);
  }

  return arr;
};

// firebase returns urls
const Gallery = async (type) => {
  // get images from firebase
  const images = await getImageUrls(type);
  const gallery = document.createElement("div");

  // for each image, create a box for it
  for (let i = 0; i < images.length; i++) {
    const imageContainer = document.createElement("div");
    imageContainer.className = "image-card";
    const img = document.createElement("img");
    img.setAttribute("src", images[i]);
    imageContainer.append(img);
    gallery.append(imageContainer);
  }

  return gallery;
};

export default Gallery;
