/**
 * TODO HREF
 */
const HomeCard = (text, image) => {
  const card = document.createElement("div");
  const img = document.createElement("img");
  const label = document.createElement("p");
  img.setAttribute("src", `/images/${image}`); // TODO
  img.className = "home-icon";
  label.textContent = text;
  label.className = "icon-label";
  card.append(img);
  card.append(label);

  return card;
};

/**
 * Home page content
 */
const Home = () => {
  const home = document.createElement("div");
  home.className = "home";
  home.append(HomeCard("traditional"), HomeCard("digital"), HomeCard("design"));

  return home;
};

export default Home;
